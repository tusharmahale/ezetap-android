package tushar.com.ezetaptest.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utility {

public static final String VISA ="visa";
public static final String MASTER ="master";
public static final String RUPAY ="rupay";
public static final String FAILED = "failed";

    public static String epocTodd(String epoc) {
        //Converting epoc time to given format
        long seconds  = Long.parseLong(epoc);
        Date d = new Date(seconds);
        DateFormat df = new SimpleDateFormat("dd-M-yyyy hh:mm:s");
       return df.format(d);

    }



}
