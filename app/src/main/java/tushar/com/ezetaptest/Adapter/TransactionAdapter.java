package tushar.com.ezetaptest.Adapter;

import android.app.Activity;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import tushar.com.ezetaptest.Model.TransactionModel;
import tushar.com.ezetaptest.R;
import tushar.com.ezetaptest.ViewHolders.TransactionViewHolder;
import tushar.com.ezetaptest.Utils.Utility;

public class TransactionAdapter extends RecyclerView.Adapter {

    ArrayList<TransactionModel> transactionModels;
    Activity activity;

   public TransactionAdapter(Activity activity)
    {
        transactionModels = new ArrayList<>();
    this.activity= activity;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(activity).inflate(R.layout.transaction_card, parent, false);
      //returing TransactionViewHolder
        return new TransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        TransactionViewHolder transactionViewHolder = (TransactionViewHolder)holder;
        TransactionModel transactionModel = transactionModels.get(position);

        transactionViewHolder.tvTransaction.setText(transactionModel.getReference());
        transactionViewHolder.txnDate.setText(Utility.epocTodd(transactionModel.getTxnDate()));


        //defining color of card based on status
        if(transactionModel.getStatus().toLowerCase().equals(Utility.FAILED.toLowerCase()))
            transactionViewHolder.cl_card.setBackgroundColor(activity.getColor(R.color.red));
        else
            transactionViewHolder.cl_card.setBackgroundColor(activity.getResources().getColor(R.color.green));

        //Defining color of cartType text based on cardType
        if(transactionModel.getCardType().toLowerCase().equals(Utility.VISA.toLowerCase()))
        {
            transactionViewHolder.tvCardType.setTextColor(activity.getColor(R.color.white));
        }
        else if(transactionModel.getCardType().toLowerCase().equals(Utility.MASTER.toLowerCase()))
        {
            transactionViewHolder.tvCardType.setTextColor(activity.getColor(R.color.colorPrimary));
        }
        else{

                transactionViewHolder.tvCardType.setTextColor(activity.getColor(R.color.colorPrimaryDark));

            }
        transactionViewHolder.tvCardType.setText(transactionModel.getCardType());
        transactionViewHolder.tvStatus.setText(transactionModel.getStatus());
        transactionViewHolder.tvAmount.setText(transactionModel.getAmount());


    }

    @Override
    public int getItemCount() {
        return transactionModels.size();
    }
    public void addAll(ArrayList<TransactionModel> transactionModels)
    {

        //Adding transaction Model to adapter
        this.transactionModels.clear();
        this.transactionModels.addAll(transactionModels);

        notifyDataSetChanged();
    }
}
