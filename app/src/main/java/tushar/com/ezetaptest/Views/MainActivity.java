package tushar.com.ezetaptest.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import tushar.com.ezetaptest.R;
/*
Created by Tushar Mahale
* */
public class MainActivity extends AppCompatActivity {
    int TIME_FOR_SPLASH = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        //startDashboard handles starts Transaction activity
        startDashboard();
    }


    public void startDashboard() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), TransactionHistoryActivity.class));
                finish();
            }
        }, TIME_FOR_SPLASH);

    }


}
