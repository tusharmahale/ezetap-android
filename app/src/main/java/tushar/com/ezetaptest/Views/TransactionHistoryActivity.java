package tushar.com.ezetaptest.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import tushar.com.ezetaptest.Adapter.TransactionAdapter;
import tushar.com.ezetaptest.Model.TransactionModel;
import tushar.com.ezetaptest.R;

public class TransactionHistoryActivity extends AppCompatActivity {

    RecyclerView rvHistory;
    ArrayList<TransactionModel> transactionModels;
    TransactionAdapter transactionAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        //Initializing references to view and object
        initRef();

        //loading data from given JSON file
        loadData();

    }

    private void initRef() {

        transactionModels = new ArrayList<>();
        rvHistory = findViewById(R.id.rv_history);
        rvHistory.setLayoutManager(new LinearLayoutManager(this));
        transactionAdapter = new TransactionAdapter(this);
        rvHistory.setAdapter(transactionAdapter);

    }

    private void loadData() {


        try {
//loading string into object
            JSONObject jsonObject = new JSONObject(loadJSONFromAsset());
            JSONArray jsonArray = jsonObject.getJSONArray("data");

          //Converting Json object to Transaction Model
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jo_inside = jsonArray.getJSONObject(i);
                TransactionModel transactionModel = new TransactionModel();
                transactionModel.setStatus(jo_inside.getString("status"));
                transactionModel.setTxnDate(jo_inside.getString("txnDate"));
                transactionModel.setAmount(jo_inside.getString("amount"));
                transactionModel.setReference(jo_inside.getString("reference"));
                transactionModel.setCardType(jo_inside.getString("cardType"));

                transactionModels.add(transactionModel);
            }

            //Adding data to transaction adapter
            transactionAdapter.addAll(transactionModels);
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("transactions.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.d("Activity ", "Returning " + json);
        return json;
    }

}
