package tushar.com.ezetaptest.ViewHolders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import tushar.com.ezetaptest.R;

public class TransactionViewHolder extends RecyclerView.ViewHolder {

    public TextView tvTransaction, tvStatus, tvAmount, txnDate, tvCardType;
    public ConstraintLayout cl_card;

    public TransactionViewHolder(@NonNull View itemView) {
        super(itemView);
        tvTransaction = itemView.findViewById(R.id.tv_reference);
        tvStatus = itemView.findViewById(R.id.tv_status);
        tvAmount = itemView.findViewById(R.id.tv_amount);
        txnDate = itemView.findViewById(R.id.tv_date);
        tvCardType = itemView.findViewById(R.id.tv_card_type);
        cl_card = itemView.findViewById(R.id.cl_cad);
    }
}
